# My Website

My personal website.

## How I deploy with

### Domain and DNS

1. Buyed a domain in [Namecheap](https://www.namecheap.com/)
2. Created a DNS zone with the same domain name in GCP: https://console.cloud.google.com/net-services/dns/zones/felipeschossler/details?project=training-gcp-cloud
3. Take the nameservers of this zone
4. Put this in a custom DNS on Namecheap
5. Create an instance allowing http and https and reserve an static external IP Address
6. Create an A dns entry pointing to this external IP Address

### Server config

1. Installed Apache: https://ubuntu.com/tutorials/install-and-configure-apache#2-installing-apache
2. Installed and config Let's Encrypt: https://certbot.eff.org/instructions?ws=apache&os=ubuntufocal

### CD

1. Install GitLab Runner: https://docs.gitlab.com/runner/install/linux-repository.html
2. Register: https://docs.gitlab.com/runner/register/

## Problems that I had

- Sudo permission to GitLab Runner: https://stackoverflow.com/questions/19383887/how-to-use-sudo-in-build-script-for-gitlab-ci
